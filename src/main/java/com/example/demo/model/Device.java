package com.example.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "device")
public class Device {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "model")
	private String model;

	@Column(name = "status")
	private String status;

	@JsonManagedReference
	@OneToMany(mappedBy = "device")
	private List<DeviceDetails> deviceDetails_Device;

	public Device(Integer id, String model, String status, List<DeviceDetails> deviceDetails_Device) {
		super();
		this.id = id;
		this.model = model;
		this.status = status;
		this.deviceDetails_Device = deviceDetails_Device;
	}

	public Device(String model, String trangThai, List<DeviceDetails> deviceDetails_Device) {
		super();
		this.model = model;
		this.status = status;
		this.deviceDetails_Device = deviceDetails_Device;
	}

	public Device() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<DeviceDetails> getDeviceDetails_Device() {
		return deviceDetails_Device;
	}

	public void setDeviceDetails_Device(List<DeviceDetails> deviceDetails_Device) {
		this.deviceDetails_Device = deviceDetails_Device;
	}

}
