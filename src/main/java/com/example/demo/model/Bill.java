package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "bill")
public class Bill {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
//ngày viết hóa đơn
	@Column(name = "invoiceDate", nullable = false)
	private Date invoiceDate;
//thanh toán
	@Column(name = "pay")
	private int pay;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "user_id")
	private USER user;

	public Bill(Integer id, Date invoiceDate, int pay, USER user) {
		super();
		this.id = id;
		this.invoiceDate = invoiceDate;
		this.pay = pay;
		this.user = user;
	}

	public Bill(Date invoiceDate, int pay, USER user) {
		super();
		this.invoiceDate = invoiceDate;
		this.pay = pay;
		this.user = user;
	}

	public Bill() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public int getPay() {
		return pay;
	}

	public void setPay(int pay) {
		this.pay = pay;
	}

	public USER getUser() {
		return user;
	}

	public void setUser(USER user) {
		this.user = user;
	}

}
