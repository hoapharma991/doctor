package com.example.demo.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "storage")
public class Storage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
//tên kho
	@Column(name = "nameStorage")
	private String nameStorage;
//ngày lưu trữ
	@Column(name = "foudingDate_Storage")
	private Date foudingDateStorage;

	@JsonManagedReference
	@OneToMany(mappedBy = "storageId")
	private List<DeviceDetails> deviceDetails_Storage;

	public Storage(Integer id, String nameStorage, Date foudingDateStorage, List<DeviceDetails> deviceDetails_Storage) {
		super();
		this.id = id;
		this.nameStorage = nameStorage;
		this.foudingDateStorage = foudingDateStorage;
		this.deviceDetails_Storage = deviceDetails_Storage;
	}

	public Storage(String nameStorage, Date foudingDateStorage, List<DeviceDetails> deviceDetails_Storage) {
		super();
		this.nameStorage = nameStorage;
		this.foudingDateStorage = foudingDateStorage;
		this.deviceDetails_Storage = deviceDetails_Storage;
	}

	public Storage() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameStorage() {
		return nameStorage;
	}

	public void setNameStorage(String nameStorage) {
		this.nameStorage = nameStorage;
	}

	public Date getFoudingDateStorage() {
		return foudingDateStorage;
	}

	public void setFoudingDateStorage(Date foudingDateStorage) {
		this.foudingDateStorage = foudingDateStorage;
	}

	public List<DeviceDetails> getDeviceDetails_Storage() {
		return deviceDetails_Storage;
	}

	public void setDeviceDetails_Storage(List<DeviceDetails> deviceDetails_Storage) {
		this.deviceDetails_Storage = deviceDetails_Storage;
	}

}
