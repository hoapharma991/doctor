package com.example.demo.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "user")
public class USER {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "username", nullable = false)
	private String userName;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "phone", nullable = false)
	private String phone;

	@Column(name = "email", nullable = false, unique = true)
	private String email;

	@Column(nullable = false)
	private String password;

	@Column(name = "image", nullable = false)
	private String image;

	@Transient
	private Date birthday;

	@Enumerated(EnumType.ORDINAL)
	private Gender gender;
	

	@Column(name = "position")
	private String position;

	@ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "USER_ID", referencedColumnName = "ID") }, inverseJoinColumns = {
					@JoinColumn(name = "ROLE_ID", referencedColumnName = "ID") })
	private List<Role> roles;

	@ManyToOne
	@JoinColumn(name = "deviceDetails_id")
	private DeviceDetails deviceDetails;

	@OneToMany(mappedBy = "user")
	private List<Bill> user_Bill;

	public USER(Integer id, String userName, String name, String phone, String email, String password, String image,
			Date birthday, Gender gender, String position, List<Role> roles, DeviceDetails deviceDetails,
			List<Bill> user_Bill) {
		super();
		this.id = id;
		this.userName = userName;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.password = password;
		this.image = image;
		this.birthday = birthday;
		this.gender = gender;
		this.position = position;
		this.roles = roles;
		this.deviceDetails = deviceDetails;
		this.user_Bill = user_Bill;
	}

	public USER(String userName, String name, String phone, String email, String password, String image, Date birthday,
			Gender gender, String position, List<Role> roles, DeviceDetails deviceDetails, List<Bill> user_Bill) {
		super();
		this.userName = userName;
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.password = password;
		this.image = image;
		this.birthday = birthday;
		this.gender = gender;
		this.position = position;
		this.roles = roles;
		this.deviceDetails = deviceDetails;
		this.user_Bill = user_Bill;
	}

	public USER(String name, String phone, String email, String image, Date birthday, Gender gender, String position,
			List<Role> roles) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.image = image;
		this.birthday = birthday;
		this.gender = gender;
		this.position = position;
		this.roles = roles;
	}

	public USER() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public DeviceDetails getDeviceDetails() {
		return deviceDetails;
	}

	public void setDeviceDetails(DeviceDetails deviceDetails) {
		this.deviceDetails = deviceDetails;
	}

	public List<Bill> getUser_Bill() {
		return user_Bill;
	}

	public void setUser_Bill(List<Bill> user_Bill) {
		this.user_Bill = user_Bill;
	}

}
