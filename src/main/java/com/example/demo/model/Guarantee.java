package com.example.demo.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "guarantee")
public class Guarantee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
//ngày xuất
	@Column(name = "polling_date", nullable = false)
	private Date pollingDate;
//ngày bảo hành
	@Column(name = "guarantee_date", nullable = false)
	private Date guaranteeDate;

	public Guarantee() {
		super();
	}

	public Guarantee(Date pollingDate, Date guaranteeDate) {
		super();
		this.pollingDate = pollingDate;
		this.guaranteeDate = guaranteeDate;
	}

	public Guarantee(Integer id, Date pollingDate, Date guaranteeDate) {
		super();
		this.id = id;
		this.pollingDate = pollingDate;
		this.guaranteeDate = guaranteeDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getPollingDate() {
		return pollingDate;
	}

	public void setPollingDate(Date pollingDate) {
		this.pollingDate = pollingDate;
	}

	public Date getGuaranteeDate() {
		return guaranteeDate;
	}

	public void setGuaranteeDate(Date guaranteeDate) {
		this.guaranteeDate = guaranteeDate;
	}

}
