package com.example.demo.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "deviceDetails")
public class DeviceDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name_Devide")
	private String nameDevice;

	@Column(name = "country")
	private String country;

	@Column(name = "seri")
	private String seri;

	@Column(name = "model")
	private String model;

	@Column(name = "price")
	private Float price;

	@Column(name = "status")
	private String status;
//ngày nhập hàng
	@Column(name = "import_Date")
	private Date importDate;
// ngày xuất hàng
	@Column(name = "polling_date")
	private Date pollingDate;

	@Column(name = "storage")
	private String storage;

	@Column(name = "guarantee_Date")
	private Date guaranteeDate;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "storage_id")
	private Storage storageId;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "device_id")
	private Device device;

	@OneToOne
	@JoinColumn(name = "guarantee_id")
	private Guarantee guarantee;

	@JsonManagedReference
	@OneToMany(mappedBy = "deviceDetails")
	private List<USER> deviceDetails_User;

	public DeviceDetails(Integer id, String nameDevice, String country, String seri, String model, Float price,
			String status, Date importDate, Date pollingDate, String storage, Date guaranteeDate, Storage storageId,
			Device device, Guarantee guarantee, List<USER> deviceDetails_User) {
		super();
		this.id = id;
		this.nameDevice = nameDevice;
		this.country = country;
		this.seri = seri;
		this.model = model;
		this.price = price;
		this.status = status;
		this.importDate = importDate;
		this.pollingDate = pollingDate;
		this.storage = storage;
		this.guaranteeDate = guaranteeDate;
		this.storageId = storageId;
		this.device = device;
		this.guarantee = guarantee;
		this.deviceDetails_User = deviceDetails_User;
	}

	public DeviceDetails(String nameDevice, String country, String seri, String model, Float price, String status,
			Date importDate, Date pollingDate, String storage, Date guaranteeDate, Storage storageId, Device device,
			Guarantee guarantee, List<USER> deviceDetails_User) {
		super();
		this.nameDevice = nameDevice;
		this.country = country;
		this.seri = seri;
		this.model = model;
		this.price = price;
		this.status = status;
		this.importDate = importDate;
		this.pollingDate = pollingDate;
		this.storage = storage;
		this.guaranteeDate = guaranteeDate;
		this.storageId = storageId;
		this.device = device;
		this.guarantee = guarantee;
		this.deviceDetails_User = deviceDetails_User;
	}

	public DeviceDetails() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNameDevice() {
		return nameDevice;
	}

	public void setNameDevice(String nameDevice) {
		this.nameDevice = nameDevice;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getSeri() {
		return seri;
	}

	public void setSeri(String seri) {
		this.seri = seri;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getImportDate() {
		return importDate;
	}

	public void setImportDate(Date importDate) {
		this.importDate = importDate;
	}

	public Date getPollingDate() {
		return pollingDate;
	}

	public void setPollingDate(Date pollingDate) {
		this.pollingDate = pollingDate;
	}

	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public Date getGuaranteeDate() {
		return guaranteeDate;
	}

	public void setGuaranteeDate(Date guaranteeDate) {
		this.guaranteeDate = guaranteeDate;
	}

	public Storage getStorageId() {
		return storageId;
	}

	public void setStorageId(Storage storageId) {
		this.storageId = storageId;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public Guarantee getGuarantee() {
		return guarantee;
	}

	public void setGuarantee(Guarantee guarantee) {
		this.guarantee = guarantee;
	}

	public List<USER> getDeviceDetails_User() {
		return deviceDetails_User;
	}

	public void setDeviceDetails_User(List<USER> deviceDetails_User) {
		this.deviceDetails_User = deviceDetails_User;
	}

}
