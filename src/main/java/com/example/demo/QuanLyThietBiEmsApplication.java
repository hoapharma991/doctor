package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuanLyThietBiEmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuanLyThietBiEmsApplication.class, args);
	}

}
