package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.USER;

public interface UserRepository extends JpaRepository<USER, Integer>{
	List<USER> findByName(String name);
	List<USER> findByPhone(String phone);
	List<USER> findByEmail(String email);
}
