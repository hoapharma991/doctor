package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Guarantee;

public interface GuaranteeRepository extends JpaRepository<Guarantee, Integer>{

}
