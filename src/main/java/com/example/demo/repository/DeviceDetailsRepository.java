package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.DeviceDetails;

public interface DeviceDetailsRepository extends JpaRepository<DeviceDetails, Integer>{

}
