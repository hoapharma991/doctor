package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Device;

public interface DeviceRepository extends JpaRepository<Device, Integer>{

}
