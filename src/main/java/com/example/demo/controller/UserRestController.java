package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.USER;
import com.example.demo.repository.UserRepository;

@RestController
@RequestMapping(value = "/api")
public class UserRestController {
	@Autowired
	UserRepository userRepository;

	@GetMapping("/users")
	public ResponseEntity<List<USER>> getAllUser(@RequestParam(required = false) String name) {
		//log infor
		try {
			List<USER> users = new ArrayList<USER>();

			if (name == null)
				userRepository.findAll().forEach(users::add);
			else
				userRepository.findByName(name).forEach(users::add);

			if (users.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(users, HttpStatus.OK);
		} catch (Exception e) {
			//log error(input value, input error, data)
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<USER> getUserlById(@PathVariable("id") Integer id) {
		Optional<USER> userData = userRepository.findById(id);

		if (userData.isPresent()) {
			return new ResponseEntity<>(userData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/users")
	public ResponseEntity<USER> createUser(@RequestBody USER user) {
		try {
			USER _user = userRepository.save(new USER(user.getName(), user.getPhone(), user.getEmail(), user.getImage(),
					user.getBirthday(), user.getGender(), user.getPosition(), user.getRoles()));
			return new ResponseEntity<>(_user, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/users/{id}")
	public ResponseEntity<USER> updateUser(@PathVariable("id") Integer id, @RequestBody USER user) {
		Optional<USER> userData = userRepository.findById(id);

		if (userData.isPresent()) {
			USER _user = userData.get();
			_user.setName(user.getName());
			_user.setPhone(user.getPhone());
			_user.setEmail(user.getEmail());
			_user.setImage(user.getImage());
			_user.setBirthday(user.getBirthday());
			_user.setGender(user.getGender());
			_user.setPosition(user.getPosition());
			_user.setRoles(user.getRoles());
			return new ResponseEntity<>(userRepository.save(_user), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") Integer id) {
		try {
			userRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/users")
	public ResponseEntity<HttpStatus> deleteAllUser() {
		try {
			userRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
//
//	@GetMapping("/users/{phone}")
//	public ResponseEntity<List<USER>> findByPhone(@PathVariable("phone") String phone) {
//		try {
//			List<USER> users = userRepository.findByPhone(phone);
//
//			if (users.isEmpty()) {
//				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//			}
//			return new ResponseEntity<>(users, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	@GetMapping("/users/{email}")
//	public ResponseEntity<List<USER>> findByEmail(@PathVariable("email") String email) {
//		try {
//			List<USER> users = userRepository.findByEmail(email);
//
//			if (users.isEmpty()) {
//				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
//			}
//			return new ResponseEntity<>(users, HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

}
