package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer{
	
	public void addCorsMapping(CorsRegistry registry) {
		registry.addMapping("/api/**")
		.allowedOriginPatterns("http://localhost:8080")
		.allowedMethods("*")
		.allowedHeaders("*")
		.allowCredentials(false)
		.maxAge(3600);
	}

}
